package id.sch.smktelkom_mlg.fragmentdyah;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivityPraktikum extends AppCompatActivity {

    Button btntoast, btnalarm, btnmaps, btnpicture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_praktikum);

        btntoast = findViewById(R.id.btntoast);
        btnalarm = findViewById(R.id.btnalarm);
        btnmaps = findViewById(R.id.btnmaps);
        btnpicture = findViewById(R.id.btnpicture);

        btntoast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = null;
                i = new Intent(MainActivityPraktikum.this, MainActivity.class);
                startActivity(i);
            }
        });
        btnalarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = null;
                i = new Intent(MainActivityPraktikum.this, Main3Activity.class);
                startActivity(i);
            }
        });
        btnmaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = null;
                i = new Intent(MainActivityPraktikum.this, Main4Activity.class);
                startActivity(i);

            }
        });
        btnpicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = null;
                i = new Intent(MainActivityPraktikum.this, Main2Activity.class);
                startActivity(i);

            }
        });
    }
}
